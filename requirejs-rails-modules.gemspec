$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "requirejs-rails-modules/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "requirejs-rails-modules"
  s.version     = RequirejsRailsModules::VERSION
  s.authors     = ["Nick Herro"]
  s.email       = ["ncherro@gmail.com"]
  s.homepage    = "http://www.ncherro.com"
  s.summary     = "Build RequireJS modules"
  s.description = "Build RequireJS modules, allowing for asset manifests."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 3.2.12"
  # s.add_dependency "jquery-rails"

  s.add_development_dependency "sqlite3"
end
