# RequireJS Rails Modules

## Under active development

This gem provides a rake task that allows you to compile RequireJS
modules containing asset manifests.

## How?

1. Create `config/requirejs.yml` (view a sample config file [here](https://bitbucket.org/ncherro/requirejs-rails-modules/wiki/Sample%20config))

2. run `rake requirejs_modules:precompile` and the modules defined in
`requirejs.yml` will be compiled and placed into your assets directory

If you're deploying via capistrano, add something like this to your
`deploy.rb` so your RequireJS modules are compiled *after* your assets
have been precompiled.

```ruby
namespace :requirejs do
  desc "Build compiled RequireJS modules"
  task :build_modules do
    rake = fetch(:rake, 'rake')
    rails_env = fetch(:rails_env, 'production')
    run "cd '#{latest_release}' && #{rake} requirejs_modules:precompile RAILS_ENV=#{rails_env}"
  end
end

# add a callback

after deploy:assets:precompile requirejs:build_modules
```

**NOTE** our rake task modifies the `manifest.yml` file created by
Sprockets during `rake assets:precompile`, so if you're running this
locally, run `rake assets:precompile` first

***

## Why?

I was working on a Rails-powered javascript widget that used
[handlebars_assets](https://github.com/leshill/handlebars_assets) to
precompile handlebars templates.

I tried using
[requirejs-rails](https://github.com/jwhitley/requirejs-rails), but it
didn't work with asset manifests as modules.

To use these assets as an AMD module, I created a manifest wrap the
templates in `define()` like this:

```
//= require hbs_templates_head
//= require_tree ./templates
  return HandlebarsTemplates;
});
```

`hbs_templates_head.js` contains this:

```
define(['hbs_manifest'], function(Handlebars) {
```

## Installation

Add this line to your Gemfile

```
gem 'requirejs-rails-modules'
```

Create and set up your `config/requirejs.yml` file

## Usage

run `rake requirejs_modules:precompile`

## License
This project uses the MIT-LICENSE
