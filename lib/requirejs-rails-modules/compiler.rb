require "json"
require "fileutils"
require "pathname"
require "yaml"

module RequirejsRailsModules
  class Compiler
    attr_accessor :config, :manifests, :build_path, :rjs_path, :assets_dir,
      :config_wrap

    def initialize
      File.open(File.join(Rails.root, 'config', 'requirejs.yml')) do |f|
        @config = YAML::load(f)
      end
      @manifests = @config.key?('manifests') ? @config.delete('manifests') : []
      @build_path = @config.delete('build_path')
      if @build_path.nil?
        raise "\n! your config/requirejs.yml file must define 'build_path'.\nbuild_path defines where the scripts will be built, relative to Rails.root\ne.g. 'vendor/assets/javascripts'"
      end
      @rjs_path = @config.delete('rjs_path')
      if @rjs_path.nil?
        raise "\n! your config/requirejs.yml file must define 'rjs_path'.\nrjs_path defines where the r.js script lives, either as an absolute path (starting with '/') or relative to the 'build_path'\ne.g. 'src/r.js'"
      else
        if @rjs_path.starts_with?('/')
          rjs_path = @rjs_path
        else
          rjs_path = File.join(Rails.root, @build_path, @rjs_path)
        end
        unless File.exists?(rjs_path)
          raise "\n! could not find the r.js file.  should exist here based on your settings\n#{rjs_path}"
        end
      end
      @assets_dir = Pathname.new(File.join(Rails.root, @build_path))
    end

    def build_config(internal=false)
      unless internal
        # loop through the rails manifests, and compile them to our build dir
        @manifests.each do |key, path|
          compiled = compile_file(path)
          @config['paths'][key] = Pathname.new(compiled).relative_path_from(@assets_dir).to_s
        end
      end

      # if there are any modules, make sure they're explicitly set in 'paths'
      if @config.key?('modules')
        @config['modules'].each do |m|
          if m.key?('create') && m.key?('include')
            # then we are dealing with a namespaced module
            m['include'].each do |inc_name|
              unless @config['paths'][inc_name]
                @config['paths'][inc_name] = Pathname.new(File.join(@assets_dir, inc_name)).relative_path_from(@assets_dir).to_s
              end
            end
          else
            # we are dealing with a regular module
            unless @config['paths'][m['name']]
              @config['paths'][m['name']] = Pathname.new(File.join(@assets_dir, m['name'])).relative_path_from(@assets_dir).to_s
            end
          end
        end
      end

      # set the @config_wrap - only used if internal, but this removes it from
      # the build_config either way
      @config_wrap = @config.delete('config_wrap')

      unless internal
        # loop through the paths, setting anything starting with '//' or
        # 'http://' or 'https://' to empty:
        @config['paths'].each do |k, v|
          @config['paths'][k] = 'empty:' unless v.match(/^(\/\/|http:\/\/|https:\/\/)/i).nil?
        end
      end

      return @config
    end

    def compile_file(filepath, opts={})
      # compile a single file, and store it in our config.requirejs.target directory
      #
      # filepath is relative to one of the asset paths
      # e.g. 'application.js' or 'libs/jquery.js'
      #
      # most of this was copied from actionpack-3.2.12/lib/sprockets/static_compiler.rb
      unless Rails.application.config.assets.enabled
        warn "Cannot precompile assets if sprockets is disabled. Please set config.assets.enabled to true"
        exit
      end

      # Ensure that action view is loaded and the appropriate
      # sprockets hooks get executed
      _ = ActionView::Base

      # use our asset config, but make it friendly for requirejs
      config = Rails.application.config
      o_compile = config.assets.compile
      config.assets.compile  = opts.key?(:compile) ? opts[:compile] : true

      o_digest = config.assets.digest
      config.assets.digest   = opts.key?(:digest) ? opts[:digest] : false

      o_compress = config.assets.compress
      config.assets.compress = opts.key?(:compress) ? opts[:compress] : false

      o_digests = config.assets.digests
      config.assets.digests = {} unless config.assets.digest

      env = opts.key?(:env) ? opts[:env] : Rails.application.assets
      target = opts.key?(:target) ? opts[:target] : File.join(Rails.root, 'tmp', 'requirejs')
      write_manifest = opts.key?(:manifest) ? opts[:manifest] : false
      compiler = Sprockets::StaticCompiler.new(
        env, # env
        target, # target
        config.assets.precompile, # paths
        :digest => config.assets.digest,
        :manifest => write_manifest
      )

      # add .js (it's omitted from the requirejs config)
      filepath += '.js' unless filepath.ends_with?('.js')
      if asset = env.find_asset(filepath)
        digest_path = compiler.write_asset(asset)

        puts "stored a compiled asset here: #{File.join(target, digest_path)}"

        # and (over)write these lines in the manifest
        if write_manifest
          manifest_filepath = File.join(target, 'manifest.yml')
          if File.exists?(manifest_filepath)
            manifest = {}
            File.open(manifest_filepath, 'r') do |f|
              manifest = YAML::load(f)
            end
            manifest[asset.logical_path] = digest_path
            manifest[compiler.aliased_path_for(asset.logical_path)] = digest_path
            puts "added #{asset.logical_path} and #{compiler.aliased_path_for(asset.logical_path)} to the manifest.yml file"
            compiler.write_manifest(manifest)
          else
            puts "No manifest was found #{manifest_filepath}"
          end
        end

        # reset Rails.application.config to original values
        config.assets.compile  = o_compile
        config.assets.digest   = o_digest
        config.assets.compress = o_compress
        config.assets.digests  = o_digests

        # return the filepath with no trailing .js
        return File.join(target, digest_path).gsub('.js', '')
      else
        raise "Could not find #{filepath}"
      end
    end

    def buildout
      # set up our instance vars, and run our manifests through Sprockets
      build_config

      # create our build file
      build = File.join(@assets_dir, 'build.js')
      File.open(build, 'w') do |f|
        # could just do @config.to_json, but this is more readable
        f.puts "(#{JSON.pretty_generate(@config)})"
      end

      build = Pathname.new(build).relative_path_from(@assets_dir)

      puts "\nCreated a build.js file here: #{build}"

      # run the build script, then delete our build file
      start_time = Time.now
      puts "Running the build script..."
      command = "cd #{@assets_dir} && node #{@rjs_path} -o #{build} && rm #{build}"
      puts "`#{command}`"
      output = `#{command}`
      if output.starts_with?('Error')
        puts "\nThere was an error running the script...\n\n"
        raise output
      end
      puts "\n#{output}\n\n"
      puts "Ran the build script in #{(Time.now - start_time).to_s} seconds"
      puts "Removed the build.js file"
      puts "Built files are here: #{@assets_dir}/#{@config['dir']}\n\n"

      # now, run the compiled modules through Sprockets and save them to our to
      # our public/assets dir
      if @config.key?('modules')
        # use the build directory as our sprockets environment
        env = Sprockets::Environment.new
        env.append_path File.join(@assets_dir, @config['dir'])
        @config['modules'].each do |m|
          compile_file(m['name'], {
            :env => env,
            :digest => Rails.application.config.assets.digest,
            :compress => Rails.application.config.assets.compress,
            :target => File.join(Rails.public_path, Rails.application.config.assets.prefix),
            :manifest => true,
          })
        end
      end
    end

    def config_to_json
      build_config(true)
      json = JSON.pretty_generate(@config.slice(
        'paths',
        'shim',
        'baseUrl',
        'waitSeconds',
        'map',
        'config',
        'packages',
        'context',
        'deps',
        'callback',
        'enforceDefine',
        'xhtml',
        'urlArgs',
        'scriptType'
      ))
      return @config_wrap.gsub('()', "(#{json})")
    end

  end

end
