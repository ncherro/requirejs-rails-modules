module RequirejsRailsModules
  class Engine < ::Rails::Engine
    if Rails.version > "3.1"
      initializer "RequirejsRailsModules precompile hook" do |app|
        app.config.assets.precompile += %w(
          requirejs-rails-modules/requirejs_config.js
        )
      end
    end
    config.generators do |g|
      g.test_framework :rspec, fixture: false
      g.fixture_replacement :factory_girl, dir: 'spec/factories'
      g.assets false
      g.helper false
    end
  end
end
