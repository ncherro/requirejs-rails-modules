require 'requirejs-rails-modules/compiler'

namespace :requirejs_modules do
  include RequirejsRailsModules

  desc "Build out your requirejs modules"
  task :precompile => :environment do
    compiler = RequirejsRailsModules::Compiler.new.buildout
  end

  desc "Config to json"
  task :config_to_json => :environment do
    puts RequirejsRailsModules::Compiler.new.config_to_json
  end

end
